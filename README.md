# Trio - Front End Test
![React](https://github.com/aleen42/badges/raw/master/src/react.svg) ![TailwindCSS](https://badges.aleen42.com/src/tailwindcss.svg) ![TailwindCSS](https://badges.aleen42.com/src/typescript.svg)

This React.js web application is part of a test for Trio.
Link: https://trio-test-app.vercel.app/

## Technical Design Document
https://docs.google.com/document/d/1Qpk8WMEqUbo81W7ZIPUdFGBSEuYLzuySGyRL32XIhlk/edit?usp=sharing

## Video
https://www.loom.com/share/299091f752fb4082b7b3fd34baeb099e

## How to run it

Install node modules:

```sh
yarn install
```

Run the app:

```sh
yarn start
```

Open this URL in your browser:

```sh
http://localhost:3000
```

## Run e2e tests

First, make sure to install supported browsers

```sh
npx playwright install
```

Run e2e tests

```sh
yarn e2e:test
```

## Features

- "Not Found" route
- Loading and Not Found animations using Lottie
- Button and dropdown animations made with TailwindCSS
- Responsive
- Accessibility (using aria-label)
- Code-Splitting (Lazy + Suspense)
- E2e tests with Playwright

## Technologies used

- React v17.0.2
- TailwindCSS
- React Router v6
- Lottie (animations)
- Playwright (e2e tests)
- Vercel: https://trio-test-app.vercel.app/

## Metrics

<img width="1268" alt="Screen Shot 2022-04-01 at 20 35 33" src="https://user-images.githubusercontent.com/2034112/161354399-0655951e-867f-48c3-ac7c-d4495772de6d.png">

## SEO

In order to have a proper SEO implementation my suggestion is to create the same web application with Next.js app or to implement prerenderer.io.

## Extras (DropDown selection)
Please check opened PR: https://github.com/buskerone/trio-test-app/pull/1 .
You can test it here: https://trio-test-app-git-feat-dropdown-selection-carlosknopel.vercel.app

### Video
https://user-images.githubusercontent.com/2034112/161577281-e9d3b6e0-ddbf-4d25-a8c3-382f3a9626f4.mov
