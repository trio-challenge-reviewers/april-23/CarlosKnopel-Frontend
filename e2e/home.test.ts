import { test, expect } from '@playwright/test';

test.describe('Home', () => {
  test.beforeEach(async ({ page }) => {
    await page.goto('http://localhost:3000');
  });

  test.afterEach(async ({ page }) => {
    await page.close();
  });

  test('Check if sync button exists', async ({ page }) => {
    await page.waitForSelector('#sync-button-container');

    const syncButtonTextElement = await page.waitForSelector('#sync-button-text');
    const syncButtonText = await syncButtonTextElement.innerText();
    
    await expect(syncButtonText).toBe('Sync Contacts');
  });

  test('Check if text changes when sync button is pressed', async ({ page }) => {
    await page.locator('#sync-button').click();

    const syncButtonTextElement = await page.waitForSelector('#sync-button-text');
    const syncButtonText = await syncButtonTextElement.innerText();
    
    await expect(syncButtonText).toBe('All Done!');
  });

  test('Check if box components exist', async ({ page }) => {
    await expect(page.locator("#box")).toHaveCount(2)
  });

  test('Check if dropdown components exist', async ({ page }) => {
    await expect(page.locator("#dropdown")).toHaveCount(2)
  });

  test('Check if box titles exist', async ({ page }) => {
    await expect(page.locator("#box-title")).toHaveCount(2)
  });

  test('Check if box descriptions exist', async ({ page }) => {
    await expect(page.locator("#box-description")).toHaveCount(2)
  });

  test('Check if abstract figure exists', async ({ page }) => {
    await expect(page.locator("#abstract-figure")).toHaveCount(1)
  });
});