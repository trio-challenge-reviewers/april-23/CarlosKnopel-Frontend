import React from 'react';
import Lottie from 'react-lottie-player';
import { LoadingAnimation } from 'assets';

const Loading = () => {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <Lottie
        loop
        animationData={LoadingAnimation}
        play
        style={{ width: 350, height: 350 }}
      />
    </div>
  );
};

export default Loading;