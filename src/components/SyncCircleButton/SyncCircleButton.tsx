import React from 'react';
import { useState } from 'react';
import { ArrowsIcon } from 'assets';
import cx from 'classnames';

const SyncCircle: React.FC = () => {
  const [clicked, setClicked] = useState(false);
  const [text, setText] = useState('Sync Contacts');

  const buttonClicked = () => {
    setClicked((clicked) => !clicked);
    setText((text) =>
      text === 'Sync Contacts' ? 'All Done!' : 'Sync Contacts'
    );
  };

  return (
    <div id="sync-button-container" className="flex flex-col justify-center items-center">
      <button
        id="sync-button"
        aria-label="sync contacts"
        onClick={buttonClicked}
        className={cx(
          'flex justify-center items-center mb-4 w-[133px] h-[133px] rounded-full bg-white transition duration-500 ease-in-out',
          {
            'rotate-180': clicked,
          }
        )}
      >
        <img alt="sync" src={ArrowsIcon} />
      </button>
      <span id="sync-button-text" className="text-gray text-lg">{text}</span>
    </div>
  );
};

export default SyncCircle;
