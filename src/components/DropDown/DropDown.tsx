import React from 'react';
import { useState } from 'react';
import { CheckmarkIcon, ArrowDownIcon, CheckmarkSmallIcon } from 'assets';
import cx from 'classnames';

const DropDown: React.FC = () => {
  const [opened, setOpened] = useState(false);
  const [contacts] = useState([
    {
      name: 'Family',
      checked: false,
    },
    {
      name: 'Work Friends',
      checked: true,
    },
    {
      name: 'Another label',
      checked: false,
    }
  ]);

  const handleDropDownClick = () => {
    setOpened((opened) => !opened);
  };

  return (
    <div id="dropdown" className={cx("w-full border-[#CBD6E2] border rounded-small", { "mb-8": opened, "mb-16": !opened })}>
      <button
        aria-label="select contacts"
        onClick={handleDropDownClick}
        className="w-full bg-[#F5F8FA] px-5 py-3"
      >
        <div className="flex justify-start items-center">
          <img alt="checkmark" src={CheckmarkIcon} />
          <span aria-label="All Contacts" className="text-left text-gray-contacts ml-4 mr-10">
            All Contacts
          </span>
          <img
            alt="arrowdown"
            src={ArrowDownIcon}
            className={cx('transition duration-300 ease-in-out', {
              'rotate-180': opened,
            })}
          />
        </div>
      </button>
      <div className={cx("transition-height duration-700 ease-in-out bg-[#F5F8FA] max-h-[111px] w-full",
          {
            "h-0": !opened,
            "h-72": opened
          }
        )
      }>
        <div className={cx("overflow-auto transition-opacity duration-700 ease-in-out h-full px-5 pb-3 pt",
            {
              "opacity-0": !opened,
              "opacity-100": opened
            }
          )
        }>
          {contacts && contacts.map((contact, key) => (
            <button aria-label={`${contact.name}`} key={key} className="flex justify-start items-center mb-3">
              <div
                className={cx(
                  'w-[14px] h-[14px] rounded-small bg-white flex justify-center items-center border border-[#7C99B6] mr-4',
                  { 'bg-[#7C99B6]': contact.checked }
                )}
              >
                {contact.checked && (
                  <img alt="contactcheck" src={CheckmarkSmallIcon} />
                )}
              </div>
              <span className="text-left text-gray-contacts">
                {contact.name}
              </span>
            </button>
          ))}
        </div>
      </div>
    </div>
  );
};

export default DropDown;
