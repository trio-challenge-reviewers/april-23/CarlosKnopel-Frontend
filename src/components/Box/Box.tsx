import React from 'react';
import { DropDown } from 'components';

interface IBoxProps {
  icon: string;
  serviceName: string;
  bodyText: string;
}

const Box: React.FC<IBoxProps> = ({ icon, serviceName, bodyText }) => {
  return (
    <div aria-label={`${serviceName} contacts`} id="box" className="min-h-[355px] md:h-[355px] max-w-[283px] flex flex-col justify-start items-center">
      <div className="md:overflow-y-visible flex flex-col justify-center items-center px-7 pt-16 text-center bg-white rounded-small border-2 border-[#DFE3EB]">
        <img alt="icon" src={icon} />
        <h3 id="box-title" className="mt-2 mb-3 text-gray font-medium text-lg">{serviceName}</h3>
        <p id="box-description" className="text-gray text-md mb-6 w-11/12">{bodyText}</p>
        <DropDown />
      </div>
    </div>
  );
};

export default Box;
