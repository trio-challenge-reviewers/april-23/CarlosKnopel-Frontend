import React from 'react';
import { SyncCircleButton, Box } from 'components';
import { AbstractForm, GmailIcon, MailchimpIcon } from 'assets';

const Home: React.FC = () => {
  return (
    <main className="w-full h-full flex justify-center items-center py-10 md:py-36">
      <section className="relative w-full flex justify-center items-start">
        <img id="abstract-figure" alt="figure" src={AbstractForm} className="absolute m-auto left-0 right-0 top-0 bottom-0" />
        <div className="relative md:gap-x-8 gap-y-6 md:gap-y-4 w-full h-full flex md:flex-row flex-col justify-center items-center">
          <Box
            icon={GmailIcon}
            serviceName="Gmail"
            bodyText="These Gmail contacts will sync to MailChimp"
          />

          <div className="h-auto md:h-[355px] flex justify-center items-center">
            <SyncCircleButton />
          </div>

          <Box
            icon={MailchimpIcon}
            serviceName="Mailchimp"
            bodyText="These Mailchimp contacts will sync to Gmail"
          />
        </div>
      </section>
    </main>
  );
};

export default Home;
