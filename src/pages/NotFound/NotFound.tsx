import React from 'react';
import Lottie from 'react-lottie-player';
import { NotFoundAnimation } from 'assets';

const NotFound = () => {
  return (
    <div className="w-full h-screen flex justify-center items-center">
      <Lottie
        loop
        animationData={NotFoundAnimation}
        play
        style={{ width: 350, height: 350 }}
      />
    </div>
  );
};

export default NotFound;