import ArrowsIcon from './arrows.svg';
import AbstractForm from './bg-abstract-form.svg';
import GmailIcon from './gmail-icon.svg';
import MailchimpIcon from './mailchimp-icon.svg';
import CheckmarkIcon from './checkmark.svg';
import ArrowDownIcon from './arrow-down.svg';
import CheckmarkSmallIcon from './checkmark-small.svg';
import NotFoundAnimation from './notfound-animation.json';
import LoadingAnimation from './loading-animation.json';

export {
  ArrowsIcon,
  AbstractForm,
  GmailIcon,
  MailchimpIcon,
  CheckmarkIcon,
  ArrowDownIcon,
  CheckmarkSmallIcon,
  NotFoundAnimation,
  LoadingAnimation
};
