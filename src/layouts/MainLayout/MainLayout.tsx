import React from 'react';

const MainLayout: React.FC = ({ children }) => {
  return <div className="md:h-screen w-full px-4 pt-6">{children}</div>;
};

export default MainLayout;
