module.exports = {
  theme: {
    extend: {
      colors: {
        'gray': '#374A5E',
        'gray-contacts': '#33475B',
      },
      fontSize: {
        'md': [
          '15px',
          {
            lineHeight: '20.43px',
          },
        ],
      },
      borderRadius: {
        'small': '2px',
      },
      transitionProperty: {
        'height': 'height'
      }
    },
  },
  plugins: [],
  content: ['./src/**/*.{js,jsx,ts,tsx,html}', './public/index.html'],
};
